﻿
namespace Aviva.FizzBuzz.BusinessLayerTests
{
    using NUnit.Framework;
    using System.Collections.Generic;
    using Aviva.FizzBuzz.EntityLayer;
    using Aviva.FizzBuzz.BusinessLayer;

    [TestFixture]
    public class BLLFizzGeneratorTest
    {
        /// <summary>
        /// Test GetNumbers Method in FizzGenerator class of Business Layer.
        /// </summary>
        [TestCase(89)] 
        [TestCase(485)] 
        [TestCase(500)]   
        [TestCase(99)]
        public void FizzGetNumbersForOutputFIZZ(int end)
        {
            var Numbers = new FizzGenerator(new Numbers(end));
            var ExpectedValue = "Fizz";
            var ActualList = new List<FizzBuzzListEntity>();

            ActualList = Numbers.GetNumbers();

            //ActualList should have "Fizz" at 8th index for these test cases.
            Assert.AreEqual(ActualList[8].Value, ExpectedValue, "Correct Output.");
        }

        /// <summary>
        /// Test GetNumbers Method in FizzGenerator class of Business Layer.
        /// </summary>
        [TestCase(89)]
        [TestCase(485)]
        [TestCase(500)]
        [TestCase(99)]
        public void FizzGetNumbersForOutput46(int end)
        {
            var Numbers = new FizzGenerator(new Numbers(end));
            var ExpectedValue = "46";
            var ActualList = new List<FizzBuzzListEntity>();

            ActualList = Numbers.GetNumbers();

            //ActualList should have 46 at 45th index for these test cases.
            Assert.AreEqual(ActualList[45].Value, ExpectedValue, "Correct Output.");
        }

        /// <summary>
        /// Test GetNumbers Method in FizzGenerator class of Business Layer.
        /// </summary>
        [TestCase(89)]
        [TestCase(485)]
        [TestCase(500)]
        [TestCase(99)]
        public void FizzGetNumbersForOutput49(int end)
        {
            var Numbers = new FizzGenerator(new Numbers(end));
            var ExpectedValue = "49";
            var ActualList = new List<FizzBuzzListEntity>();

            ActualList = Numbers.GetNumbers();

            //ActualList should have 49 at 48th index for these test cases.
            Assert.AreEqual(ActualList[48].Value, ExpectedValue, "Correct Output.");
        }
    }
}
