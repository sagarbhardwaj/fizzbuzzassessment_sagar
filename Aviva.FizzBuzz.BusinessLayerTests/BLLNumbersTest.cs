﻿
namespace Aviva.FizzBuzz.BusinessLayerTests
{
    using NUnit.Framework;
    using System.Collections.Generic;
    using Aviva.FizzBuzz.EntityLayer;
    using Aviva.FizzBuzz.BusinessLayer;

    /// <summary>
    /// Summary description for BLLNumbersTests
    /// </summary>
    [TestFixture]
    public class BLLNumbersTest
    {
        /// <summary>
        /// Test GetNumbers Method of Numbers class in Business Layer for parameters 35.
        /// </summary>
        [Test]
        public void NumbersGetNumbersForOutput13()
        {
            var Numbers = new Numbers(35);
            var ExpectedValue = "13";
            var ActualList = new List<FizzBuzzListEntity>();
            ActualList = Numbers.GetNumbers();
            Assert.AreEqual(ActualList[12].Value, ExpectedValue, "Correct Output.");
        }

        /// <summary>
        /// Test GetNumbers Method of Numbers class in Business Layer for parameters 45.
        /// </summary>
        [Test]
        public void NumbersGetNumbersForOutput43()
        {
            var Numbers = new Numbers(45);
            var ExpectedValue = "43";
            var ActualList = new List<FizzBuzzListEntity>();
            ActualList = Numbers.GetNumbers();
            Assert.AreEqual(ActualList[42].Value, ExpectedValue, "Correct Output.");
        }

        /// <summary>
        /// Test GetNumbers Method of Numbers class in Business Layer for parameters 5.
        /// </summary>
        [Test]
        public void NumbersGetNumbersForOutput5()
        {
            var Numbers = new Numbers(5);
            var ExpectedValue = "5";
            var ActualList = new List<FizzBuzzListEntity>();
            ActualList = Numbers.GetNumbers();
            Assert.AreEqual(ActualList[4].Value, ExpectedValue, "Correct Output.");
        }

        /// <summary>
        /// Test GetNumbers Method of Numbers class in Business Layer for parameters 94.
        /// </summary>
        [Test]
        public void NumbersGetNumbersForOutput89()
        {
            var Numbers = new Numbers(94);
            var ExpectedValue = "89";
            var ActualList = new List<FizzBuzzListEntity>();
            ActualList = Numbers.GetNumbers();
            Assert.AreEqual(ActualList[88].Value, ExpectedValue, "Correct Output.");
        }

        /// <summary>
        /// Test GetNumbers Method of Numbers class in Business Layer for parameters 135.
        /// </summary>
        [Test]
        public void NumbersGetNumbersForOutput121()
        {
            var Numbers = new Numbers(135);
            var ExpectedValue = "121";
            var ActualList = new List<FizzBuzzListEntity>();
            ActualList = Numbers.GetNumbers();
            Assert.AreEqual(ActualList[120].Value, ExpectedValue, "Correct Output.");
        }

        /// <summary>
        /// Test GetNumbers Method of Numbers class in Business Layer for parameters 960.
        /// </summary>
        [Test]
        public void NumbersGetNumbersForOutput960()
        {
            var Numbers = new Numbers(960);
            var ExpectedValue = "960";
            var ActualList = new List<FizzBuzzListEntity>();
            ActualList = Numbers.GetNumbers();
            Assert.AreEqual(ActualList[959].Value, ExpectedValue, "Correct Output.");
        }
    }
}
