﻿
namespace Aviva.FizzBuzz.BusinessLayerTests
{
    using NUnit.Framework;
    using System.Collections.Generic;
    using Aviva.FizzBuzz.BusinessLayer;
    using Aviva.FizzBuzz.EntityLayer;

    /// <summary>
    /// Test all the methods of FizzBuzzPageLogic class in Business Layer.
    /// </summary>
    [TestFixture]
    public class BLLFizzBuzzPageLogicTest
    {
        /// <summary>
        /// Test FindRecords method of FizzBuzzPageLogic class in Business Layer for i/p : 78.
        /// </summary>
        [Test]
        public void FindRecordsForUserInput78()
        {
            var ExpectedValue = "Fizz";
            var FizzBuzzPageLogic = new FizzBuzzPageLogic();
            List<FizzBuzzListEntity> ActualList = new List<FizzBuzzListEntity>();
            FizzBuzzEntity FizzBuzzEntity = new FizzBuzzEntity();
            FizzBuzzEntity.UserInput = 78;

            ActualList = FizzBuzzPageLogic.FindRecords(FizzBuzzEntity);

            Assert.AreEqual(ActualList[8].Value, ExpectedValue, "Correct Output.");
        }

        /// <summary>
        /// Test FindRecords method of FizzBuzzPageLogic class in Business Layer for i/p : 45.
        /// </summary>
        [Test]
        public void FindRecordsForUserInput45()
        {
            var ExpectedValue = "Fizz Buzz";
            var FizzBuzzPageLogic = new FizzBuzzPageLogic();
            var ActualList = new List<FizzBuzzListEntity>();
            var FizzBuzzEntity = new FizzBuzzEntity();
            FizzBuzzEntity.UserInput = 45;


            ActualList = FizzBuzzPageLogic.FindRecords(FizzBuzzEntity);

            Assert.AreEqual(ActualList[44].Value, ExpectedValue, "Correct Output.");
        }

        /// <summary>
        /// Test FindRecords method of FizzBuzzPageLogic class in Business Layer for i/p : 28.
        /// </summary>
        [Test]
        public void FindRecordsForUserInput28()
        {
            var ExpectedValue = "16";
            var FizzBuzzPageLogic = new FizzBuzzPageLogic();
            var ActualList = new List<FizzBuzzListEntity>();
            var FizzBuzzEntity = new FizzBuzzEntity();
            FizzBuzzEntity.UserInput = 28;

            ActualList = FizzBuzzPageLogic.FindRecords(FizzBuzzEntity);

            Assert.AreEqual(ActualList[15].Value, ExpectedValue, "Correct Output.");
        }

        /// <summary>
        /// Test FindRecords method of FizzBuzzPageLogic class in Business Layer for i/p : 61.
        /// </summary>
        [Test]
        public void FindRecordsForUserInput61()
        {
            var ExpectedValue = "52";
            var FizzBuzzPageLogic = new FizzBuzzPageLogic();
            var ActualList = new List<FizzBuzzListEntity>();
            var FizzBuzzEntity = new FizzBuzzEntity();
            FizzBuzzEntity.UserInput = 61;

            ActualList = FizzBuzzPageLogic.FindRecords(FizzBuzzEntity);

            Assert.AreEqual(ActualList[51].Value, ExpectedValue, "Correct Output.");
        }
    }
}
