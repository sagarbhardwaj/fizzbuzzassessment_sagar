﻿
namespace Aviva.FizzBuzz.BusinessLayerTests
{
    using NUnit.Framework;
    using System.Collections.Generic;
    using Aviva.FizzBuzz.BusinessLayer;
    using Aviva.FizzBuzz.EntityLayer;

    /// <summary>
    /// Summary description for BLLWizzGeneratorTest
    /// </summary>
    [TestFixture]
    public class BLLWizzGeneratorTest
    {
        /// <summary>
        /// Test GetNumbers Method in WizzGenerator class of Business Layer.
        /// </summary>
        [TestCase(89)] 
        [TestCase(485)] 
        [TestCase(500)]   
        [TestCase(99)]   
        public void WizzGetNumbersInputForWIZZ(int end)
        {
            var Numbers = new WizzGenerator(new Numbers(end));
            var ExpectedValue = "Wizz";
            var ActualList = new List<FizzBuzzListEntity>();

            ActualList = Numbers.GetNumbers();

            //ActualList should have "Wizz" at 8th index for these test cases.
            Assert.AreEqual(ActualList[8].Value, ExpectedValue, "Correct Output.");
        }

        /// <summary>
        /// Test GetNumbers Method in WizzGenerator class of Business Layer.
        /// </summary>
        [TestCase(89)]
        [TestCase(485)]
        [TestCase(500)]
        [TestCase(99)]
        public void WizzGetNumbersForOutput4(int end)
        {
            var Numbers = new WizzGenerator(new Numbers(end));
            var ExpectedValue = "4";
            var ActualList = new List<FizzBuzzListEntity>();

            ActualList = Numbers.GetNumbers();

            //ActualList should have 4 at 3th index for these test cases.
            Assert.AreEqual(ActualList[3].Value, ExpectedValue, "Correct Output.");
        }

        /// <summary>
        /// Test GetNumbers Method in WizzGenerator class of Business Layer.
        /// </summary>
        [TestCase(89)]
        [TestCase(485)]
        [TestCase(500)]
        [TestCase(99)]
        public void WizzGetNumbersForOutput91(int end)
        {
            var Numbers = new WizzGenerator(new Numbers(end));
            var ExpectedValue = "91";
            var ActualList = new List<FizzBuzzListEntity>();

            ActualList = Numbers.GetNumbers();

            //ActualList should have 91 at 90th index for these test cases.
            Assert.AreEqual(ActualList[90].Value, ExpectedValue, "Correct Output.");
        }
    }

}
