﻿
namespace Aviva.FizzBuzz.BusinessLayerTests
{
    using System.Collections.Generic;
    using Aviva.FizzBuzz.BusinessLayer;
    using Aviva.FizzBuzz.EntityLayer;
    using NUnit.Framework;

    [TestFixture]
    public class BLLBuzzGeneratorTest
    {
        /// <summary>
        /// Test GetNumbers Method in BuzzGenerator class of Business Layer.
        /// </summary>
        [TestCase(230)] 
        [TestCase(365)]
        [TestCase(110)]   
        [TestCase(70)]
        public void BuzzGetNumbersForOutputBuzz(int end)
        {
            var Numbers = new BuzzGenerator(new Numbers(end));
            var ExpectedValue = "Buzz";
            var ActualList = new List<FizzBuzzListEntity>();
            
            ActualList = Numbers.GetNumbers();
            
            //ActualList should have "Buzz" at 9th index for these test cases.
            Assert.AreEqual(ActualList[9].Value, ExpectedValue, "Correct Output.");
        }

        /// <summary>
        /// Test GetNumbers Method in BuzzGenerator class of Business Layer.
        /// </summary>
        [TestCase(230)]
        [TestCase(365)]
        [TestCase(110)]
        [TestCase(70)]
        public void BuzzGetNumbersForOutput14(int end)
        {
            var Numbers = new BuzzGenerator(new Numbers(end));
            var ExpectedValue = "14";
            var ActualList = new List<FizzBuzzListEntity>();

            ActualList = Numbers.GetNumbers();

            //ActualList should have "Buzz" at 13th index for these test cases.
            Assert.AreEqual(ActualList[13].Value, ExpectedValue, "Correct Output.");
        }

        /// <summary>
        /// Test GetNumbers Method in BuzzGenerator class of Business Layer.
        /// </summary>
        [TestCase(230)]
        [TestCase(365)]
        [TestCase(110)]
        [TestCase(70)]
        public void BuzzGetNumbersForOutput46(int end)
        {
            var Numbers = new BuzzGenerator(new Numbers(end));
            var ExpectedValue = "46";
            var ActualList = new List<FizzBuzzListEntity>();

            ActualList = Numbers.GetNumbers();

            //ActualList should have "Buzz" at 45th index for these test cases.
            Assert.AreEqual(ActualList[45].Value, ExpectedValue, "Correct Output.");
        }


        /// <summary>
        /// Test GetNumbers Method in BuzzGenerator class of Business Layer.
        /// </summary>
        [TestCase(230)]
        [TestCase(365)]
        [TestCase(110)]
        [TestCase(70)]
        public void BuzzGetNumbersForOutput19(int end)
        {
            var Numbers = new BuzzGenerator(new Numbers(end));
            var ExpectedValue = "19";
            var ActualList = new List<FizzBuzzListEntity>();

            ActualList = Numbers.GetNumbers();

            //ActualList should have "Buzz" at 18th index for these test cases.
            Assert.AreEqual(ActualList[18].Value, ExpectedValue, "Correct Output.");
        }
    }
}
