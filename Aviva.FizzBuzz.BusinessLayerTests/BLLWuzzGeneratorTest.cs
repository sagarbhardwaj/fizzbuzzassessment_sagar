﻿
namespace Aviva.FizzBuzz.BusinessLayerTests
{
    using NUnit.Framework;
    using System.Collections.Generic;
    using Aviva.FizzBuzz.BusinessLayer;
    using Aviva.FizzBuzz.EntityLayer;
   
    [TestFixture]
    public class BLLWuzzGeneratorTest
    {
        /// <summary>
        /// Test GetNumbers Method in WuzzGenerator class of Business Layer.
        /// </summary>
        [TestCase(230)] 
        [TestCase(365)]
        [TestCase(110)]  
        [TestCase(70)]  
        public void WuzzGetNumbersInputForWUZZ(int end)
        {
            var Numbers = new WuzzGenerator(new Numbers(end));
            var ExpectedValue = "Wuzz";
            var ActualList = new List<FizzBuzzListEntity>();

            ActualList = Numbers.GetNumbers();

            //ActualList should have "Wuzz" at 14th index for these test cases.
            Assert.AreEqual(ActualList[14].Value, ExpectedValue, "Correct Output.");
        }

        /// <summary>
        /// Test GetNumbers Method in WuzzGenerator class of Business Layer.
        /// </summary>
        [TestCase(230)]
        [TestCase(365)]
        [TestCase(110)]
        [TestCase(70)]
        public void WuzzGetNumbersForOutput94(int end)
        {
            var Numbers = new WuzzGenerator(new Numbers(end));
            var ExpectedValue = "94";
            var ActualList = new List<FizzBuzzListEntity>();

            ActualList = Numbers.GetNumbers();

            //ActualList should have 94 at 93th index for these test cases.
            Assert.AreEqual(ActualList[93].Value, ExpectedValue, "Correct Output.");
        }

        /// <summary>
        /// Test GetNumbers Method in WuzzGenerator class of Business Layer.
        /// </summary>
        [TestCase(230)]
        [TestCase(365)]
        [TestCase(110)]
        [TestCase(70)]
        public void WuzzGetNumbersForOutput89(int end)
        {
            var Numbers = new WuzzGenerator(new Numbers(end));
            var ExpectedValue = "89";
            var ActualList = new List<FizzBuzzListEntity>();

            ActualList = Numbers.GetNumbers();

            //ActualList should have 89 at 88th index for these test cases.
            Assert.AreEqual(ActualList[88].Value, ExpectedValue, "Correct Output.");
        }
    }
}
