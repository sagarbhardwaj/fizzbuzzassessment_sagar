﻿

namespace Aviva.FizzBuzz.BusinessLayerTests
{
    using NUnit.Framework;
    using System.Collections.Generic;
    using Aviva.FizzBuzz.EntityLayer;
    using Aviva.FizzBuzz.BusinessLayer;

    [TestFixture]
    public class BLLFizzBuzzGeneratorTest
    {
        /// <summary>
        /// Test GetNumbers Method in FizzBuzzGenerator class of Business Layer.
        /// </summary>
        [TestCase(789)] 
        [TestCase(401)] 
        [TestCase(365)] 
        [TestCase(110)]   
        [TestCase(60)]   
        public void FizzBuzzGetNumbersForOutputFIZZBUZZ(int start, int end)
        {
            var Numbers = new FizzBuzzGenerator(new Numbers(end));
            var ExpectedValue = "Fizz Buzz";
            var ActualList = new List<FizzBuzzListEntity>();
            
            ActualList = Numbers.GetNumbers();

            //ActualList should have "Fizz-Buzz" at 14th index for these test cases.
            Assert.AreEqual(ActualList[14].Value, ExpectedValue, "Correct Output.");
        }

        /// <summary>
        /// Test GetNumbers Method in FizzBuzzGenerator class of Business Layer.
        /// </summary>
        [TestCase(789)]
        [TestCase(401)]
        [TestCase(365)]
        [TestCase(110)]
        [TestCase(60)]
        public void FizzBuzzGetNumbersForOutput46(int start, int end)
        {
            var Numbers = new FizzBuzzGenerator(new Numbers(end));
            var ExpectedValue = "46";
            var ActualList = new List<FizzBuzzListEntity>();

            ActualList = Numbers.GetNumbers();

            //ActualList should have 46 at 45th index for these test cases.
            Assert.AreEqual(ActualList[45].Value, ExpectedValue, "Correct Output.");
        }

        /// <summary>
        /// Test GetNumbers Method in FizzBuzzGenerator class of Business Layer.
        /// </summary>
        [TestCase(789)]
        [TestCase(401)]
        [TestCase(365)]
        [TestCase(110)]
        [TestCase(60)]
        public void FizzBuzzGetNumbersForOutput23(int start, int end)
        {
            var Numbers = new FizzBuzzGenerator(new Numbers(end));
            var ExpectedValue = "23";
            var ActualList = new List<FizzBuzzListEntity>();

            ActualList = Numbers.GetNumbers();

            //ActualList should have 22 at 22th index for these test cases.
            Assert.AreEqual(ActualList[22].Value, ExpectedValue, "Correct Output.");
        }
    }
}
