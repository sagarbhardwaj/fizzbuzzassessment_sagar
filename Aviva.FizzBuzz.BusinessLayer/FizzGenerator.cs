﻿
namespace Aviva.FizzBuzz.BusinessLayer
{
    using System;
    using System.Collections.Generic;
    using Aviva.FizzBuzz.EntityLayer;

    /// <summary>
    /// This class is generating Fizz at the required places.
    /// </summary>
    public class FizzGenerator : Generator
    {
        public FizzGenerator(IFizzBuzzGenerator IFizzBuzzGenerator)
        {
            this.iFizzBuzzGenerator = IFizzBuzzGenerator;
        }

        /// <summary>
        /// Overrides the Decorator class method to generate Fizz
        /// </summary>
        /// <returns>It returns a list after updating Fizz.</returns>
        public override List<FizzBuzzListEntity> GetNumbers()
        {
            var numberItems = new List<FizzBuzzListEntity>();
            foreach (var number in this.iFizzBuzzGenerator.GetNumbers())
            {
                number.Value = (number.iteration % 3 == 0) ? "Fizz" : number.Value;
                number.StyleClass = (number.iteration % 3 == 0) ? "BlueFizz" : number.StyleClass;

                numberItems.Add(number);
            }

            return numberItems;
        }
    }
}
