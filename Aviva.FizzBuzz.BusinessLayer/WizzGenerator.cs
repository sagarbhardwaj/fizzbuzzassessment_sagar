﻿
namespace Aviva.FizzBuzz.BusinessLayer
{
    using System;
    using System.Collections.Generic;
    using Aviva.FizzBuzz.EntityLayer;

    /// <summary>
    /// This class is generating Wizz at the required places.
    /// </summary>
    public class WizzGenerator : Generator
    {
        public WizzGenerator(IFizzBuzzGenerator IFizzBuzzGenerator)
        {
            this.iFizzBuzzGenerator = IFizzBuzzGenerator;
        }

        /// <summary>
        /// Overrides the Decorator class method to generate Wizz
        /// </summary>
        /// <returns>It returns a list after updating Wizz.</returns>
        public override List<FizzBuzzListEntity> GetNumbers()
        {
            var numberItems = new List<FizzBuzzListEntity>();

            foreach (var number in this.iFizzBuzzGenerator.GetNumbers())
            {
                if (DateTime.Now.DayOfWeek == DayOfWeek.Wednesday)
                {
                    number.Value = (number.iteration % 3 == 0) ? "Wizz" : number.Value;
                    number.StyleClass = (number.iteration % 3 == 0) ? "BlueFizz" : number.StyleClass;
                }

                numberItems.Add(number);
            }

            return numberItems;
        }
    }
}
