﻿
namespace Aviva.FizzBuzz.BusinessLayer
{
    using System.Collections.Generic;
    using Aviva.FizzBuzz.EntityLayer;

    /// <summary>
    /// Interface to implement methods in the FizzBuzzPageLogic class.
    /// </summary>
    public interface IFizzBuzzPageLogic
    {
        List<FizzBuzzListEntity> FindRecords(FizzBuzzEntity FizzBuzzEntity);
    }
}
