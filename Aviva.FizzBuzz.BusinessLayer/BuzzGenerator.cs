﻿
namespace Aviva.FizzBuzz.BusinessLayer
{
    using System;
    using System.Collections.Generic;
    using Aviva.FizzBuzz.EntityLayer;

    /// <summary>
    /// This class is generating Buzz at the required places.
    /// </summary>
    public class BuzzGenerator : Generator
    {
        public BuzzGenerator(IFizzBuzzGenerator IFizzBuzzGenerator)
        {
            this.iFizzBuzzGenerator = IFizzBuzzGenerator;
        }

        /// <summary>
        /// Overrides the Decorator class method to generate Buzz
        /// </summary>
        /// <returns>It returns a list after updating Buzz.</returns>
        public override List<FizzBuzzListEntity> GetNumbers()
        {
            var numberItems = new List<FizzBuzzListEntity>();
            foreach (var number in this.iFizzBuzzGenerator.GetNumbers())
            {
                number.Value = (number.iteration % 5 == 0) ? "Buzz" : number.Value;
                number.StyleClass = (number.iteration % 5 == 0) ? "GreenBuzz" : number.StyleClass;

                numberItems.Add(number);
            }

            return numberItems;
        }
    }
}
