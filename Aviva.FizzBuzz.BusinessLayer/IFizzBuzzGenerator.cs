﻿
namespace Aviva.FizzBuzz.BusinessLayer
{
    using System.Collections.Generic;
    using Aviva.FizzBuzz.EntityLayer;

    /// <summary>
    /// Interface for implementing methods in Decorator & Number.
    /// </summary>
    public interface IFizzBuzzGenerator
    {
        List<FizzBuzzListEntity> GetNumbers();
    }
}
