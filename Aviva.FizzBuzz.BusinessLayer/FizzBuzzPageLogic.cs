﻿
namespace Aviva.FizzBuzz.BusinessLayer
{
    using System.Collections.Generic;
    using Aviva.FizzBuzz.EntityLayer;

    /// <summary>
    /// This class implements the IFizzBuzzPageLogic interface.
    /// </summary>
    public class FizzBuzzPageLogic : IFizzBuzzPageLogic
    {
        /// <summary>
        /// This method Generates the Fizz Buzz Series based on the user input.
        /// </summary>
        /// <param name="FizzBuzzEntity">Object of the FizzBuzzEntity class.</param>
        /// <returns>List of type ListFizzBuzzEntity values.</returns>
        public List<FizzBuzzListEntity> FindRecords(FizzBuzzEntity FizzBuzzEntity)
        {
            var Number = new FizzBuzzGenerator(new BuzzGenerator(new FizzGenerator(new Numbers(FizzBuzzEntity.UserInput))));
            return Number.GetNumbers();
        }
    }
}
