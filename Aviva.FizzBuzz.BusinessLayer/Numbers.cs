﻿

namespace Aviva.FizzBuzz.BusinessLayer
{
    using System;
    using System.Collections.Generic;
    using Aviva.FizzBuzz.EntityLayer;

    /// <summary>
    /// This class is generating Numbers at the required places.
    /// </summary>
    public class Numbers : IFizzBuzzGenerator
    {
        private readonly int end;

        public Numbers(int? end)
        {
            this.end = Convert.ToInt16(end);
        }

        /// <summary>
        /// Implements the interface method to generate Numbers.
        /// </summary>
        /// <returns>It returns a list after inserting numbers into it.</returns>
        public List<FizzBuzzListEntity> GetNumbers()
        {
            var NumberItems = new List<FizzBuzzListEntity>();
            for (var i = 1; i <= this.end; i++)
            {
                var NumberItem = new FizzBuzzListEntity()
                {
                    iteration = i,
                    Value = i.ToString(),
                    StyleClass = string.Empty
                };
                NumberItems.Add(NumberItem);
            }

            return NumberItems;
        }
    }
}
