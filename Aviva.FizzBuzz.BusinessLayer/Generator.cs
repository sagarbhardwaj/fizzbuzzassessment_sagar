﻿
namespace Aviva.FizzBuzz.BusinessLayer
{
    using System.Collections.Generic;
    using Aviva.FizzBuzz.EntityLayer;

    public abstract class Generator : IFizzBuzzGenerator
    {
        protected IFizzBuzzGenerator iFizzBuzzGenerator;
        public abstract List<FizzBuzzListEntity> GetNumbers();
    }
}
