﻿
namespace Aviva.FizzBuzz.BusinessLayer
{
    using System;
    using System.Collections.Generic;
    using Aviva.FizzBuzz.EntityLayer;

    /// <summary>
    /// This class is generating Fizz Buzz at the required places.
    /// </summary>
    public class FizzBuzzGenerator : Generator
    {
        public FizzBuzzGenerator(IFizzBuzzGenerator IFizzBuzzGenerator)
        {
            this.iFizzBuzzGenerator = IFizzBuzzGenerator;
        }

        public override List<FizzBuzzListEntity> GetNumbers()
        {
            var numberItems = new List<FizzBuzzListEntity>();
            foreach (var number in this.iFizzBuzzGenerator.GetNumbers())
            {
                number.Value = (number.iteration % 15 == 0) ? "Fizz Buzz" : number.Value;
                number.StyleClass = (number.iteration % 15 == 0) ? string.Empty : number.StyleClass;

                numberItems.Add(number);
            }

            return numberItems;
        }
    }
}
