﻿
namespace Aviva.FizzBuzz.BusinessLayer
{
    using System;
    using System.Collections.Generic;
    using Aviva.FizzBuzz.EntityLayer;

    /// <summary>
    /// This class is generating Wuzz at the required places.
    /// </summary>
    public class WuzzGenerator : Generator
    {
        public WuzzGenerator(IFizzBuzzGenerator IFizzBuzzGenerator)
        {
            this.iFizzBuzzGenerator = IFizzBuzzGenerator;
        }

        /// <summary>
        /// Overrides the Decorator class method to generate Wuzz
        /// </summary>
        /// <returns>It returns a list after updating Wuzz.</returns>
        public override List<FizzBuzzListEntity> GetNumbers()
        {
            var numberItems = new List<FizzBuzzListEntity>();
            foreach (var number in this.iFizzBuzzGenerator.GetNumbers())
            {
                if (DateTime.Now.DayOfWeek == DayOfWeek.Wednesday)
                {
                    number.Value = (number.iteration % 5 == 0) ? "Wuzz" : number.Value;
                    number.StyleClass = (number.iteration % 5 == 0) ? "GreenBuzz" : number.StyleClass;
                }

                numberItems.Add(number);
            }

            return numberItems;
        }
    }
}
