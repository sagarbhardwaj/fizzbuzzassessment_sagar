﻿
namespace Aviva.FizzBuzz.Presentation.Models
{
    public class FizzBuzzListModel
    {
        public string Value { get; set; }
        public string StyleClass { get; set; }
    }
}