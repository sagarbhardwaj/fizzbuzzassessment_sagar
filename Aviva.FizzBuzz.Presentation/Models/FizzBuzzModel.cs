﻿
namespace Aviva.FizzBuzz.Presentation.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    //This model has all the properties to handle for the Index View.
    public class FizzBuzzModel
    {
        [Display(Name = "Please enter the Number")]
        [Required(ErrorMessage = "Cannot Be Left Blank.")]
        [Range(1, 1000, ErrorMessage = "Enter number between 1 to 1000")]
        public int? UserInput { get; set; }
        public List<FizzBuzzListModel> PageRecords { get; set; }
    }
}