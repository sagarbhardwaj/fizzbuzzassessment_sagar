﻿
namespace Aviva.FizzBuzz.Presentation.Controllers
{
    using AutoMapper;
    using StructureMap;
    using System.Web.Mvc;
    using Aviva.FizzBuzz.BusinessLayer;
    using Aviva.FizzBuzz.EntityLayer;
    using Aviva.FizzBuzz.Presentation.Models;

    public class FizzBuzzSeriesController : Controller
    {
        protected IFizzBuzzPageLogic IFizzBuzzPageLogic { get; set; }

        //Non Parameterised constructor
        ////public FizzBuzzSeriesController()
        ////{
            
        ////}

        //Parameterised constructor used for the mocking the results for inner methods during unit testing.
        public FizzBuzzSeriesController(IFizzBuzzPageLogic iFizzBuzzPageLogic)
        {
            this.IFizzBuzzPageLogic = iFizzBuzzPageLogic;
        }


        [HttpGet]
        public ActionResult FizzBuzzView()
        {
            return this.View();
        }

        [HttpPost]
        public ActionResult FizzBuzzView(FizzBuzzModel FizzBuzzModel)
        {            
            if (this.ModelState.IsValid)
            {
                var UserInput = FizzBuzzModel.UserInput;

                //PRG Pattern (Post-Redirect-Get)
                return RedirectToAction("FizzBuzzViewPRG", "FizzBuzzSeries", new { UserInput });
            }

            return this.View();
        }

        [HttpGet]
        public ActionResult FizzBuzzViewPRG(int UserInput)
        {
            var FizzBuzzModel = new FizzBuzzModel();
            FizzBuzzModel.UserInput = UserInput;

            //Converting FizzBuzzEntity List to FizzBuzzModel List.
            Mapper.CreateMap<FizzBuzzModel, FizzBuzzEntity>();
            var FizzBuzzEntity = Mapper.Map<FizzBuzzModel, FizzBuzzEntity>(FizzBuzzModel);

            //Calling the PageRecords method to get the series.
            FizzBuzzEntity.PageRecords = this.IFizzBuzzPageLogic.FindRecords(FizzBuzzEntity);

            //Converting FizzBuzzEntity List to FizzBuzzModel List.
            if (FizzBuzzEntity != null)
            {
                Mapper.CreateMap<FizzBuzzEntity, FizzBuzzModel>();
                Mapper.CreateMap<FizzBuzzListEntity, FizzBuzzListModel>();

                FizzBuzzModel = Mapper.Map<FizzBuzzEntity, FizzBuzzModel>(FizzBuzzEntity);
            }

            return this.View("FizzBuzzView", FizzBuzzModel);
        }
    }
}
