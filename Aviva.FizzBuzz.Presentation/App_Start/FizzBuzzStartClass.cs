﻿using Aviva.FizzBuzz.BusinessLayer;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


[assembly: WebActivator.PreApplicationStartMethod(typeof(Aviva.FizzBuzz.Presentation.App_Start.FizzBuzzStartClass), "Start")]
namespace Aviva.FizzBuzz.Presentation.App_Start
{
    public class FizzBuzzStartClass
    {
        public static void Start()
        {
            ObjectFactory.Configure(x =>
            {
                x.For<IFizzBuzzGenerator>().Use<Numbers>().Named("NumberGenerator");
                x.For<IFizzBuzzGenerator>().Use<FizzBuzzGenerator>().Named("FizzBuzzGenerator");
                x.For<IFizzBuzzGenerator>().Use<WizzGenerator>().Named("WizzGenerator");
                x.For<IFizzBuzzGenerator>().Use<WuzzGenerator>().Named("WuzzGenerator");
                x.For<IFizzBuzzGenerator>().Use<FizzGenerator>().Named("FizzGenerator");
                x.For<IFizzBuzzGenerator>().Use<BuzzGenerator>().Named("BuzzGenerator");
                x.For<IFizzBuzzPageLogic>().Use<FizzBuzzPageLogic>();
            });
        }
    }
}