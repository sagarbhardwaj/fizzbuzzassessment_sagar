﻿
namespace Aviva.FizzBuzz.PresentationTests
{
    using Moq;
    using NUnit.Framework;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using Aviva.FizzBuzz.BusinessLayer;
    using Aviva.FizzBuzz.EntityLayer;
    using Aviva.FizzBuzz.Presentation.Controllers;
    using Aviva.FizzBuzz.Presentation.Models;
    

    [TestFixture]
    public class FizzBuzzSeriesControllerTest
    {
        [Test]
        public void FizzBuzzSeriesControllerIndexInputAs90()
        {
            var mockListFizzBuzzEntity = new FizzBuzzListEntity
            {          
                iteration = 2,
                StyleClass = string.Empty,
                Value = "2"
            };

            var mockFizzBuzzEntity = new FizzBuzzEntity
            {
                UserInput = 90,
                PageRecords = new List<FizzBuzzListEntity> { mockListFizzBuzzEntity }
            };
            
            //Mocking FindRecords method output.
            var mockFizzBuzzLogic = new Mock<IFizzBuzzPageLogic>();
            mockFizzBuzzLogic.Setup(m => m.FindRecords(It.IsAny<FizzBuzzEntity>()));
            var objFizzBuzzSeriesController = new FizzBuzzSeriesController(mockFizzBuzzLogic.Object);
            
            //Initializing expected output.
            var objFizzBuzzModel = new FizzBuzzModel();
            objFizzBuzzModel.UserInput = 90;
            objFizzBuzzModel.PageRecords = null;

            //Calling method.
            var actual = objFizzBuzzSeriesController.FizzBuzzView(objFizzBuzzModel) as ViewResult;
            var actualModel = actual.Model as FizzBuzzModel;
            Assert.IsTrue(actualModel.UserInput == 90);
        }

        [Test]
        public void FizzBuzzSeriesControllerIndexInputAs35()
        {
            var mockListFizzBuzzEntity = new FizzBuzzListEntity
            {
                iteration = 2,
                StyleClass = string.Empty,
                Value = "2"
            };

            var mockFizzBuzzEntity = new FizzBuzzEntity
            {
                UserInput = 35,
                PageRecords = new List<FizzBuzzListEntity> { mockListFizzBuzzEntity }
            };
            
            //Mocking FindRecords method output.
            var mockFizzBuzzLogic = new Mock<IFizzBuzzPageLogic>();
            mockFizzBuzzLogic.Setup(m => m.FindRecords(It.IsAny<FizzBuzzEntity>()));
            var objFizzBuzzSeriesController = new FizzBuzzSeriesController(mockFizzBuzzLogic.Object);

            //Initializing expected output.
            var objFizzBuzzModel = new FizzBuzzModel();
            objFizzBuzzModel.UserInput = 35;
            objFizzBuzzModel.PageRecords = null;

            //Calling method.
            var actual = objFizzBuzzSeriesController.FizzBuzzView(objFizzBuzzModel) as ViewResult;
            var actualModel = actual.Model as FizzBuzzModel;
            Assert.IsTrue(actualModel.UserInput == 35);
        }

        [Test]
        public void FizzBuzzSeriesControllerIndexInputAs125()
        {
            var mockListFizzBuzzEntity = new FizzBuzzListEntity
            {
                iteration = 2,
                StyleClass = string.Empty,
                Value = "2"
            };

            var mockFizzBuzzEntity = new FizzBuzzEntity
            {
                UserInput = 125,
                PageRecords = new List<FizzBuzzListEntity> { mockListFizzBuzzEntity }
            };

            //Mocking FindRecords method output.
            var mockFizzBuzzLogic = new Mock<IFizzBuzzPageLogic>();
            mockFizzBuzzLogic.Setup(m => m.FindRecords(It.IsAny<FizzBuzzEntity>()));
            var objFizzBuzzSeriesController = new FizzBuzzSeriesController(mockFizzBuzzLogic.Object);

            //Initializing expected output.
            var objFizzBuzzModel = new FizzBuzzModel();
            objFizzBuzzModel.UserInput = 125;
            objFizzBuzzModel.PageRecords = null;

            //Calling method.
            var actual = objFizzBuzzSeriesController.FizzBuzzView(objFizzBuzzModel) as ViewResult;
            var actualModel = actual.Model as FizzBuzzModel;
            Assert.IsTrue(actualModel.UserInput == 125);
        }

        [Test]
        public void FizzBuzzSeriesControllerIndexInputAs15()
        {
            var mockListFizzBuzzEntity = new FizzBuzzListEntity
            {
                iteration = 2,
                StyleClass = string.Empty,
                Value = "2"
            };

            var mockFizzBuzzEntity = new FizzBuzzEntity
            {
                UserInput = 15,
                PageRecords = new List<FizzBuzzListEntity> { mockListFizzBuzzEntity }
            };

            //Mocking FindRecords method output.
            var mockFizzBuzzLogic = new Mock<IFizzBuzzPageLogic>();
            mockFizzBuzzLogic.Setup(m => m.FindRecords(It.IsAny<FizzBuzzEntity>()));
            var objFizzBuzzSeriesController = new FizzBuzzSeriesController(mockFizzBuzzLogic.Object);

            //Initializing expected output.
            var objFizzBuzzModel = new FizzBuzzModel();
            objFizzBuzzModel.UserInput = 15;
            objFizzBuzzModel.PageRecords = null;

            //Calling method.
            var actual = objFizzBuzzSeriesController.FizzBuzzView(objFizzBuzzModel) as ViewResult;
            var actualModel = actual.Model as FizzBuzzModel;
            Assert.IsTrue(actualModel.UserInput == 15);
        }


        [Test]
        public void MOQ_FizzBuzzViewPRG()
        {
            //Create a mock object of a MailClient class which implements IMailClient
            var mockFizzBuzzLogic = new Moq.Mock<IFizzBuzzPageLogic>();


            var mockListFizzBuzzEntity = new FizzBuzzListEntity
            { iteration = 2, StyleClass = string.Empty, Value = "2" };
            var mockFizzBuzzEntity = new FizzBuzzEntity
            { UserInput = 15, PageRecords = new List<FizzBuzzListEntity> { mockListFizzBuzzEntity }};
            

            mockFizzBuzzLogic.Setup(m => m.FindRecords(It.IsAny<FizzBuzzEntity>()));


            var objFizzBuzzSeriesController = new FizzBuzzSeriesController(mockFizzBuzzLogic.Object);

        }
    }
}
