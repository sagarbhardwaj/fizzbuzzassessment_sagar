﻿
namespace Aviva.FizzBuzz.EntityLayer
{
    using System.Collections.Generic;

    /// <summary>
    /// Entity class to hold input & final list values for the required data.
    /// </summary>
    public class FizzBuzzEntity
    {
        //Creating enitities for Business Layer & to avoid circular reference with Presentation layer.
        public int? UserInput { get; set; }
        public List<FizzBuzzListEntity> PageRecords { get; set; }
    }
}
