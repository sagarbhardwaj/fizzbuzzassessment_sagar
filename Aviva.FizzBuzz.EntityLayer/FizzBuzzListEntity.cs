﻿
namespace Aviva.FizzBuzz.EntityLayer
{
    /// <summary>
    /// Entity class to hold final list values for the required data.
    /// </summary>
    public class FizzBuzzListEntity
    {
        public int iteration { get; set; }
        public string Value { get; set; }
        public string StyleClass { get; set; }
    }
}
